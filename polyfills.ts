
import 'core-js/es6';
import 'core-js/es7/reflect';
require( 'zone.js/dist/zone' );

if ( process.env.ENV === 'production' ) {
    // Production
} else {
    // Development and test
    Error['stackTraceLimit'] = Infinity;
    // require( 'zone.js/dist/long-stack-trace-zone' );
}
// To set env:
// on SET NODE_ENV=production
// in JS file --> process.env.NODE_ENV = 'production';
// in package.json -->  "start": "set NODE_ENV=dev && ..."
