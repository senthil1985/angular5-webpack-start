
let path = require('path');
let webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

// build optimization
let ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
// bundle Visualization - optional
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

// to clean Dist dir prior to every build
const CleanWebpackPlugin = require('clean-webpack-plugin');

// can be used for conditionals
const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = {
    context: __dirname, // to automatically find tsconfig.json
    entry: {
        polyfills: './polyfills.ts',
        app: './main.ts'
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].[hash].bundle.js'
    },
    cache: true,    // improve increment build times - include only those files which are changed.
    stats: 'errors-only',
    module: {

        rules: [
            // ts-loader: convert typescript (es6) to javascript (es6),
            // babel-loader: converts javascript (es6) to javascript (es5)
            // Loaders always execute right to left
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/,
                options: {
                    // build Optimization: disable type checker - we will use it in fork plugin
                    transpileOnly: true
                }
            }
        ]
    },

    resolve: {
        extensions: ['.js', '.ts', '.tsx']
    },

    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /[\\/]node_modules[\\/]/,
                    chunks: 'initial',
                    name: 'vendor'
                },
            }
        }
    },
    watchOptions: {
        ignored: /node_modules/
    },
    plugins: [
        new CleanWebpackPlugin(['dist/*.*'], { watch: true }),
        new ForkTsCheckerWebpackPlugin(),    // build optimization
        new HtmlWebPackPlugin({
            template: './index.tmpl.html',
            filename: '../index.html',
            title: 'Product Management',
            // chunksSortMode: 'auto'
            chunksSortMode: function (chunk1: any, chunk2: any) {
                let orders = ['polyfills', 'vendor', 'app'];
                let order1 = orders.indexOf(chunk1.names[0]);
                let order2 = orders.indexOf(chunk2.names[0]);
                if (order1 > order2) {
                    return 1;
                } else if (order1 < order2) {
                    return -1;
                } else {
                    return 0;
                }
            }
        }),
        new BundleAnalyzerPlugin({
            analyzerMode: 'static',
            reportFilename: '../report.html',
            openAnalyzer: false // do not launch on every build, launch manually
        })
    ]

};
