export class Product {
    constructor(
        public _id?: any,
        public productId?: number,
        public productName?: string,
        public productCode?: string,
        public releaseDate?: string,
        public description?: string,
        public price?: number,
        public starRating?: number,
        public imageUrl?: string
    ) {
        // set the default value
        this.imageUrl = 'http://www.freeiconspng.com/uploads/no-image-icon-23.jpg';
        // set the default value for product code which will act as prefix
        // TODO: write a method to get new product code (will be autoincremented)
        this.productCode = 'GMG-';
    }
}
