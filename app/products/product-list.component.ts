import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { IProduct } from './product.interface';
import { Observable } from 'rxjs/Observable';
const ProdURL = 'http://localhost:8000/products/';
@Component({
    selector: 'pm-products',
    templateUrl: 'app/products/product-list.component.html',
    styleUrls: ['app/products/product-list.component.css']
})

export class ProductListComponent implements OnInit {
    pageTitle = 'Products List';
    listFilter: string;
    hideFilter =  false;
    products: IProduct[];
    productsAsync: Observable<IProduct[]>;

    constructor(private http: HttpClient) {}

    ngOnInit(): void {
        // this.http.get<IProduct[]>('http://localhost:8000/products', {observe: 'response'}).subscribe((res) => {
        //     console.log(res);
        //     this.products = res.body;
        // })

        this.loadProducts();
    }

    loadProducts() {
        this.productsAsync = this.http.get<IProduct[]>(ProdURL);
    }

    toggleImage(): void {
        alert('hey it works!')
        this.products[0].starRating = 5;
    }

    onClickStar(e: any) {
        alert('clicked' + e);
    }

    onProdDel(product: IProduct) {
        if (confirm('Are you sure you want to delete?')) {
            this.http.delete(`${ProdURL}${product._id}`).subscribe((res) => {
                this.loadProducts();
            });
        }
    }
}
