import { Pipe, PipeTransform} from '@angular/core';
import { IProduct } from './product.interface';

@Pipe({
    name: 'productFilter'
})

export class ProductFilter implements PipeTransform {
    transform(value: IProduct[], filterBy: string) {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;

        return filterBy ?
            value.filter((product: IProduct) => product.productName.toLocaleLowerCase().indexOf(filterBy) !== -1 )
            : value;
    }
}
