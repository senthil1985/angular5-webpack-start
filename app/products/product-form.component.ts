import { Component, OnInit, HostListener } from '@angular/core';
import { Product } from './product.model';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
const ProductURL = 'http://localhost:8000/products/';

@Component( {
    selector: 'product-form',
    templateUrl: '/app/products/product-form.component.html',
    styleUrls: ['app/products/product-form.component.css']
} )


export class ProductFormComponent implements OnInit {
    model: Product = new Product();
    originalProductModel: Product;
    isEditMode: boolean;
    pageTitle: string;
    saveSuccess: boolean;

    constructor(private activatedRoute: ActivatedRoute, private http: HttpClient) { }

    onFormSave(formInstance: NgForm): void {
        let headers = new HttpHeaders({'Content-Type': 'application/json'});
        if (this.isEditMode) {
            // PUT call
            this.http.put(`${ProductURL}${this.model._id}`, this.model, {headers}).subscribe((res) => {
                console.log(res);
                this.saveSuccess = true;
            });
        } else {
            //  POST call
            this.http.post(`${ProductURL}`, this.model, {headers})
                .subscribe((res) => {
                    this.saveSuccess = true;
                })
        }
    }

    ngOnInit() {
        this.pageTitle = 'Add Product';
        this.activatedRoute.params.subscribe((params) => {
            console.log(params);
            this.isEditMode = params['id'] ? true : false;
            if (this.isEditMode) {
                this.pageTitle = 'Edit Product';
                this.getProduct(params['id']);
            }
        });
    }

    getProduct(id: any) {
        this.http.get(`${ProductURL}${id}`).subscribe((res) => {
            this.model = res;
            this.originalProductModel = Object.assign( {}, this.model );
        })
    }
    @HostListener('window:beforeunload')
    canDeactivate(): boolean {
        let hasModelChanged = JSON.stringify(this.model) !== JSON.stringify(this.originalProductModel);
        if (hasModelChanged) {
            return confirm('Discard changes?');
        } else {
            return true;
        }
    }
}

