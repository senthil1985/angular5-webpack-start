export interface IProduct {
    _id?: any,
    productId?: number,
    productName?: string,
    productCode?: string,
    releaseDate?: string,
    description?: string,
    price?: number,
    starRating?: number,
    imagerUrl?: string
}
