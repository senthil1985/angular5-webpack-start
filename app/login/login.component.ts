import {Component, OnInit} from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import { emailDomainValidator } from '../shared/validators/emailDomain.validator';
import { UserService } from '../shared/services/user.service';

@Component({
    selector: 'login-form',
    templateUrl: 'app/login/login.component.html',
    styleUrls: ['app/login/login.component.css']
})

export class LoginComponent implements OnInit {
    myControl: FormControl;
    userFormGroup: FormGroup;
    loginError: boolean;
    constructor(private fb: FormBuilder, private userSvc: UserService) { }

    /* For nesting:
    personal: {
        name: '',
        contact: {
            business: '',
            home: ''
        }
    }
    personal = new FormGroup({
        name: new FormControl(''),
        contact: new FormGroup({
            business: new FormControl(''),
            home: new FormControl('')
        })
    })
    */
    ngOnInit() {
        // this.myControl = new FormControl('Jatin Maini');
        this.userFormGroup = this.fb.group({
            email: ['', [Validators.required, emailDomainValidator]],
            password: ['', Validators.required]
        });
    }

    onLogin(): void {
        console.log(this.userFormGroup);
        if (!this.userSvc.loginUser(this.userFormGroup.value) ) {
            this.loginError = true;
        }
    }

}
