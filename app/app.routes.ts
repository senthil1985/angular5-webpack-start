import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProductListComponent } from './products/product-list.component';
import { ProductFormComponent } from './products/product-form.component';
import { AuthGuard } from './shared/guards/auth.guard';
import { DeactivateGuard } from './shared/guards/deactivate.guard';

export const routes: Routes = [
    {path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent, canActivate: [AuthGuard]},
    {path: 'home', component: ProductListComponent, canActivate: [AuthGuard]},
    {path: 'add-product', component: ProductFormComponent, canActivate: [AuthGuard], canDeactivate: [DeactivateGuard]},
    {path: 'edit-product/:id', component: ProductFormComponent, canActivate: [AuthGuard], canDeactivate: [DeactivateGuard]},
    {path: '**', component: LoginComponent, canActivate: [AuthGuard]}
]
