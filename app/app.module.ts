import { NgModule } from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent }  from './app.component';
import { ProductListComponent } from './products/product-list.component';
import { BoldHoverDirective } from './shared/directives/boldHover.directive';
import { StarComponent } from './shared/components/star.component';
import { ProductFilter } from './products/product-list.pipe';
import { ProductFormComponent } from './products/product-form.component';
import { LoginComponent } from './login/login.component';
import { UserService } from './shared/services/user.service';
import {RouterModule} from '@angular/router';
import { routes } from './app.routes';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';
import { AuthGuard } from './shared/guards/auth.guard';
import { DeactivateGuard } from './shared/guards/deactivate.guard';

@NgModule({
  imports: [ BrowserModule,  FormsModule, ReactiveFormsModule, HttpClientModule, RouterModule.forRoot(routes)],
  declarations: [ AppComponent, ProductListComponent,
                  BoldHoverDirective, StarComponent, ProductFilter, ProductFormComponent, LoginComponent ],
  bootstrap: [ AppComponent ],
  providers: [UserService, AuthGuard, DeactivateGuard,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }]
})
export class AppModule { }
