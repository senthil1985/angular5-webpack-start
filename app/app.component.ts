import { Component, OnInit } from '@angular/core';
import { UserService } from './shared/services/user.service';



@Component({
    selector: 'pm-app',
    templateUrl: 'app/app.component.html'
})
export class AppComponent implements OnInit {
    loggedIn: boolean;

    constructor(private userSvc: UserService) {}

    ngOnInit() {
        this.loggedIn = this.userSvc.isUserLoggedIn();
        this.userSvc.loginStatusChange.subscribe((status: boolean) => {
            this.loggedIn = status;
        });
    }
    getTitle(): string {
        return 'Product Management';
    }
    logout() {
        this.userSvc.logout();
    }
}
