import {Component, Input, Output, EventEmitter, OnChanges, SimpleChanges} from '@angular/core';

@Component({
    selector: 'ai-star',
    templateUrl: '/app/shared/components/star.component.html',
    styleUrls: ['app/shared/components/star.component.css']
})

export class StarComponent implements OnChanges {
    @Input() rating: number;
    @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();

    getStars = function(rating: any) {
        // Get the value
        let val = parseFloat(rating);
        // Turn value into percentage - this will be the width of container.
        let size = (val / 5) * 100;   // 5 because max 5 stars
        return size;
    }

    onStarClick = function() {
        this.ratingClicked.emit('Rating clicked' + this.rating);
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log('input prop changed!');
    }
}
