import {Injectable} from '@angular/core';
import { CanDeactivate } from '@angular/router';

@Injectable()

export class DeactivateGuard implements CanDeactivate<any> {
    canDeactivate(component: any) {
        return component.canDeactivate ? component.canDeactivate() : true;
    }
}
