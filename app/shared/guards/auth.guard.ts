import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { UserService } from '../services/user.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private userSvc: UserService, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let activate = this.userSvc.isUserLoggedIn();
        let url: string = state.url;

        console.log('we are passing thru the guard');
        console.log(activate, url);

        if (url === '' || url === '/login') {
            if (activate) {
                console.log('user already logged in');
                this.router.navigate(['home']);
            }
        } else {
            if (!activate) {
                this.router.navigate(['login']);
            }
        }

        return true;
    }
}
