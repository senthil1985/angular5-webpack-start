import {FormControl} from '@angular/forms';

export function emailDomainValidator(control: FormControl){
    const email = control.value;
    if (email) {
        if (email.indexOf("@") !== -1){
            let domain = email.split('@');
            if(domain[1] !== 'gmail.com') {
                return {
                    emailDomain: {
                        parsedDomain: domain,
                        reqdDomain: 'Gmail'
                    }
                }
            }
        }else{
            return {
                emailFormat: {
                    email: email,
                    format: 'Must have @'
                }
            }
        }
    }
    // no errors
    return null;
}
