import {Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()

export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authRequest = req.clone({
            headers: req.headers.set('Authorization', 'SOME_DYNAMIC_TOKEN')
        });

        return next.handle(authRequest);
    }
}
