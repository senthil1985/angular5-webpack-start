import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';


@Injectable()

export class UserService {
    user: any;
    redirectUrl: any;
    @Output() loginStatusChange: EventEmitter<any> = new EventEmitter();

    constructor( private router: Router ) {
        if ( !this.getUser() ) {
            this.resetUser();
        }
    }
    private resetUser () {
        return this.user = {
            name: '',
            loggedIn: false,
            email: '', password: ''
        };
    }
    private setUser ( user: any ) {
        localStorage.setItem( 'user', JSON.stringify( user ) );
    }
    public loginUser ( user: any ): boolean {
        if ( user.email === 'test@gmail.com' && user.password === 'password' ) {
            user.loggedIn = true;
            this.setUser( user );
            this.loginStatusChange.emit( true );
            this.router.navigateByUrl( '/home' );

            return true;
        } else {
            return false;
        }
    }

    public getUser (): any {
        let obj = localStorage.getItem( 'user' );
        if ( obj ) {
            this.user = JSON.parse( obj );
        };

        return this.user;
    }

    public isUserLoggedIn (): boolean {
        let user = this.getUser();
        return user.loggedIn;
    }

    public logout (): void {

        let user = this.resetUser();
        this.setUser( user );
        this.loginStatusChange.emit( false );
        this.router.navigateByUrl( 'login' );

    }
}

