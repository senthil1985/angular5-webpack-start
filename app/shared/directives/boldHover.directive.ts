import {Directive, ElementRef} from '@angular/core';

@Directive({
    selector: '[boldHover]',
    host: {
        '(mouseenter)': 'onMouseEnter()',
        '(mouseleave)': 'onMouseLeave()',
    }
})

export class BoldHoverDirective {
    private element: HTMLElement;

    constructor(el: ElementRef) {
        this.element = el.nativeElement;
    }

    onMouseEnter() {
        this.element.style.fontWeight = 'bold';
    }

    onMouseLeave() {
        this.element.style.fontWeight = 'normal';
    }
}
